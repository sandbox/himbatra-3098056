<?php

namespace Drupal\skylight\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class SimpleGlossaryHelpController.
 *
 * @package Drupal\simple_glossary\Controller
 */
class SkylightHelpController extends ControllerBase {

  /**
   * Method Content to help tab.
   */
  public function content() {
    return SkylightHelpController::helperFetchHelpContent();
  }

  /**
   * Helper Method to fetch template content.
   */
  public function helperFetchHelpContent() {
    return [
      '#theme' => 'help_tab_view',
      '#help_data' => [],
    ];
  }

}
